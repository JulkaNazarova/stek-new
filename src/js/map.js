if ($("#map").length > 0) {

  ymaps.ready(init);

  function init() {
    var myMap = new ymaps.Map("map", {
        center: [55.76, 37.64],
        zoom: 10
      }),

      // Создаем метку с помощью вспомогательного класса.
      myPlacemark1 = new ymaps.Placemark([55.8, 37.6], {
        // Свойства.
        // Содержимое иконки, балуна и хинта.
        iconContent: '1',
        balloonContent: 'Балун',
        hintContent: 'Стандартный значок метки'
      }, {
        // Опции.
        // Стандартная фиолетовая иконка.
        preset: 'twirl#violetIcon'
      }),

      myPlacemark2 = new ymaps.Placemark([55.76, 37.56], {
        // Свойства.
        hintContent: 'Собственный значок метки'
      }, {
        // Опции.
        // Своё изображение иконки метки.
        iconImageHref: 'images/myIcon.gif',
        // Размеры метки.
        iconImageSize: [30, 42],
        // Смещение левого верхнего угла иконки относительно
        // её "ножки" (точки привязки).
        iconImageOffset: [-3, -42]
      });

    // Добавляем все метки на карту.
    myMap.geoObjects
      .add(myPlacemark1);
    // .add(myGeoObject);
  }


}

